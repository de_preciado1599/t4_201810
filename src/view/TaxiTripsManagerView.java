package view;

import java.util.Scanner;

import controller.Controller;
import model.vo.Taxi;
import model.logic.OrderedArrayMaxPQ;
import model.logic.TaxiTripsManager;
import model.vo.Empresa;
import model.vo.Service;

public class TaxiTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				//imprime menu
				printMenu();

				//opcion req
				int option1 = sc.nextInt();

				switch(option1)
				{
				//1C cargar informacion dada
				case 1:

					//imprime menu cargar
					printMenuCargar();

					//opcion cargar
					int optionCargar = sc.nextInt();

					//directorio json
					String linkJson = "";
					String fechaIni = "";
					String fechaFin = "";
					switch (optionCargar)
					{
					//direccion json pequeno
					case 1:

						linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
						System.out.println("Recuerde el formato de fecha -- 2017-02-01T09:00:00.000 -- \n");
						System.out.println("por favor ingrese la fecha inicial en que desea buscar los taxis \n");
						fechaIni = sc.next();
						System.out.println("por favor ingrese la fecha Final en que desea buscar los taxis \n");
						fechaFin = sc.next();
						break;

						//direccion json mediano
					case 2:

						linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
						System.out.println("Recuerde el formato de fecha -- 2017-02-01T09:00:00.000 -- \n");
						System.out.println("por favor ingrese la fecha inicial en que desea buscar los taxis \n");
						fechaIni = sc.next();
						System.out.println("por favor ingrese la fecha Final en que desea buscar los taxis \n");
						fechaFin = sc.next();
						break;

						//direccion json grande
					case 3:

						linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
						System.out.println("Recuerde el formato de fecha -- 2017-02-01T09:00:00.000 -- \n");
						System.out.println("por favor ingrese la fecha inicial en que desea buscar los taxis \n");
						fechaIni = sc.next();
						System.out.println("por favor ingrese la fecha Final en que desea buscar los taxis \n");
						fechaFin = sc.next();
						break;
					}

					//Memoria y tiempo
					long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					long startTime = System.nanoTime();

					//Cargar data
					Controller.loadServices(linkJson, fechaIni, fechaFin);

					//Tiempo en cargar
					long endTime = System.nanoTime();
					long duration = (endTime - startTime)/(1000000);

					//Memoria usada
					long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

					break;}
				break;
			case 2:
				Taxi [] resultadoOrden = Controller.servicesInOrder();
				System.out.println(resultadoOrden);
				for(int i = 0; i< resultadoOrden.length;i++) 
				{
					String id = resultadoOrden[i].getTaxiId();
					int numeroServicios = resultadoOrden[i].getServiciosRegistrados();
					System.out.println("imprimiendo en orden \n"+"id:"+id+" numServ:"+numeroServicios);
				}


				break;
			case 3:
				//TO DO sincronizar con el controlador y a su vez con el metodo del modelo
				OrderedArrayMaxPQ<Empresa> resultado = Controller.servicesPriorityQueue();
				while(resultado.size()!=0){
					Empresa actual = resultado.delMax();
					if(!actual.getCompany().equals("Independent Owner")){
						System.out.println("El nombre de la compa��a es =" + actual.getCompany() + " El n�mero de servicios son =" +actual.getServiciosRegistrados());
					}
				}


				break;
			case 4:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cargar los servicios de un taxi reportados en un subconjunto de datos");
		System.out.println("2. ordenar arreglo de taxis (HeapSort)");
		System.out.println("3. colaDePrioridad ");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}
	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}
}
