package controller;

import api.ITaxiTripsManager;
import model.logic.OrderedArrayMaxPQ;
import model.logic.TaxiTripsManager;
import model.vo.Taxi;
import model.vo.Empresa;
import model.vo.Service;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	/** To load the services of the taxi with taxiId */
	public static void loadServices( String ServiceFile, String fechaIni, String FechaFin ) {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-medium.json";
		
		manager.loadServices( serviceFile, fechaIni,FechaFin );
	}
	
	public static Taxi [] servicesInOrder() {
		return manager.servicesInOrder();
	}
	
	public static OrderedArrayMaxPQ<Empresa> servicesPriorityQueue(){
		return manager.colaPrioridadTaxis();
	}
}
