package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.Comparator;

import javax.swing.plaf.metal.MetalIconFactory.FolderIcon16;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import javafx.scene.control.TabPane.TabClosingPolicy;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Lista;
import model.vo.Taxi;
import model.vo.Empresa;
import model.vo.Service;
import model.vo.Servicio;

public class TaxiTripsManager implements ITaxiTripsManager {
	//_________________________________________________________________________________________________
	// Atributos
	//_________________________________________________________________________________________________

	/**
	 * arreglo para almacenar la informacion de los taxis
	 */
	private Taxi[] taxis;

	//_________________________________________________________________________________________________
	// Constantes
	//_________________________________________________________________________________________________

	/**
	 * 
	 */
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";

	/**
	 * 
	 */
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";

	/**
	 * 
	 */
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";


	/**
	 * 
	 */

	public Boolean loadServices(String serviceFile, String pFechaIni, String pFechaFin) 
	{
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with File:" + serviceFile);
		boolean rta= false;
		if(serviceFile.equals(DIRECCION_LARGE_JSON))
		{
			int x= 2;
			String route = "./data/taxi-trips-wrvz-psew-subset-0"+x+"-02-2017.json";
			while(x<9){
				loadFileJson2(route, pFechaIni, pFechaFin);
				x++;
			}
		}
		else
		{
			loadFileJson(serviceFile, pFechaIni, pFechaFin);
			rta= true;
		}
		return rta;

	}

	/**
	 * 
	 */
	public void loadFileJson(String file, String pFechaIni, String pFechaFin){
		try{
			BufferedReader bf = new BufferedReader(new FileReader(new File(file)));
			Gson gson = new GsonBuilder().create();
			Service[] service = gson.fromJson(bf, Service[].class);
			Lista <Taxi> listaTaxis = new Lista<Taxi>();
			int contador =0;
			String fechaIniActual = "";
			String fechaFinActual = "";
			for (Service service2 : service) 
			{
				contador ++;
				System.out.println("-----------en marcha---------- "+"iteracion"+contador+" tamaniolista/" +listaTaxis.size()+"\n ");
				Taxi aAgregar = null;
				fechaIniActual = service2.getTrip_start_timestamp();
				fechaFinActual = service2.getTrip_end_timestamp();
				//se pone una fecha para evitar excepciones, se garantiza que esta fecha es inexistente
				if(fechaIniActual == null) 
				{
					fechaIniActual = "2030-02-01T20:30:00.000";
				}
				if(fechaFinActual == null) 
				{
					fechaFinActual = "2030-02-01T20:30:00.000";
				}
				System.out.println("-------fechas------ "+"inipara"+pFechaIni+" finParam" +pFechaFin+"--act"+"ini"+fechaIniActual+" fin"+fechaFinActual+"\n ");
				if(service2.getTaxi_id().compareTo("")!=0) 
				{
					if(service2.getCompany() != null && service2.getCompany().compareTo("")==0) 
					{
						aAgregar = new Taxi(service2.getTaxi_id(), "Independent Owner");
					}
					else
					{
						aAgregar = new Taxi(service2.getTaxi_id(), service2.getCompany());
					}

				}
				if(listaTaxis.size()==0) 
				{
					if(aAgregar != null && pFechaFin.compareTo(fechaFinActual)>=0 && pFechaIni.compareTo(fechaIniActual)<=0)
					{
						aAgregar.setServiciosRegistrados();
						aAgregar.setDineroServicios(service2.getTrip_total());
						aAgregar.setDistanciaServicios(service2.getTrip_miles());
						listaTaxis.add(aAgregar);
					}
				}	
				else
				{
					if(aAgregar == null) 
					{
						aAgregar = new Taxi("excepcion", "ninguna");
					}
					Taxi buscado = listaTaxis.get(aAgregar);
					if(buscado != null && pFechaFin.compareTo(fechaFinActual)>=0 && pFechaIni.compareTo(fechaIniActual)<=0)
					{
						buscado.setServiciosRegistrados();
						buscado.setDineroServicios(service2.getTrip_total());
						buscado.setDistanciaServicios(service2.getTrip_miles());
					}
					else if (pFechaFin.compareTo(fechaFinActual)>=0 && pFechaIni.compareTo(fechaIniActual)<=0)
					{
						aAgregar.setServiciosRegistrados();
						aAgregar.setDineroServicios(service2.getTrip_total());
						aAgregar.setDistanciaServicios(service2.getTrip_miles());
						listaTaxis.add(aAgregar);
					}	
					//					Taxi temp = aAgregar;
					//					listaTaxis.delete(aAgregar);
					//					listaTaxis.add(temp);
				}
			}
			llenarArreglo(listaTaxis);
			bf.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 *
	 */
	public void loadFileJson2(String file, String pFechaIni, String pFechaFin){
		try{
			JsonReader jsonReader = new JsonReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			Gson gson = new GsonBuilder().create();
			jsonReader.beginArray();
			Lista <Taxi> listaTaxis = new Lista<Taxi>();
			int contador =0;
			String fechaIniActual = "";
			String fechaFinActual = "";
			while (jsonReader.hasNext())
			{
				Service service2 = gson.fromJson(jsonReader, Service.class);
				contador ++;
				System.out.println("-----------en marcha---------- "+"iteracion"+contador+" tamaniolista/" +listaTaxis.size()+"\n ");
				Taxi aAgregar = null;
				fechaIniActual = service2.getTrip_start_timestamp();
				fechaFinActual = service2.getTrip_end_timestamp();
				//se pone una fecha para evitar excepciones, se garantiza que esta fecha es inexistente
				if(fechaIniActual == null) 
				{
					fechaIniActual = "2030-02-01T20:30:00.000";
				}
				if(fechaFinActual == null) 
				{
					fechaFinActual = "2030-02-01T20:30:00.000";
				}
				System.out.println("-------fechas------ "+"inipara"+pFechaIni+" finParam" +pFechaFin+"--act"+"ini"+fechaIniActual+" fin"+fechaFinActual+"\n ");
				if(service2.getTaxi_id().compareTo("")!=0) 
				{
					if(service2.getCompany() != null && service2.getCompany().compareTo("")==0) 
					{
						aAgregar = new Taxi(service2.getTaxi_id(), "Independent Owner");
					}
					else
					{
						aAgregar = new Taxi(service2.getTaxi_id(), service2.getCompany());
					}

				}
				if(listaTaxis.size()==0) 
				{
					if(aAgregar != null && pFechaFin.compareTo(fechaFinActual)>=0 && pFechaIni.compareTo(fechaIniActual)<=0)
					{
						aAgregar.setServiciosRegistrados();
						aAgregar.setDineroServicios(service2.getTrip_total());
						aAgregar.setDistanciaServicios(service2.getTrip_miles());
						listaTaxis.add(aAgregar);
					}
				}	
				else
				{
					Taxi buscado = listaTaxis.get(aAgregar);
					if(buscado != null && pFechaFin.compareTo(fechaFinActual)>=0 && pFechaIni.compareTo(fechaIniActual)<=0)
					{
						aAgregar.setServiciosRegistrados();
						aAgregar.setDineroServicios(service2.getTrip_total());
						aAgregar.setDistanciaServicios(service2.getTrip_miles());
					}
					else if (pFechaFin.compareTo(fechaFinActual)>=0 && pFechaIni.compareTo(fechaIniActual)<=0)
					{
						aAgregar.setServiciosRegistrados();
						aAgregar.setDineroServicios(service2.getTrip_total());
						aAgregar.setDistanciaServicios(service2.getTrip_miles());
					}
					Taxi temp = aAgregar;
					listaTaxis.delete(aAgregar);
					listaTaxis.add(temp);
					System.out.println("///tamanio///" + listaTaxis.get(temp).getServiciosRegistrados());
				}
			}
			llenarArreglo(listaTaxis);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * metodoAuxiliar que llena el arreglo
	 */
	public void llenarArreglo(Lista<Taxi> pLista) 
	{
		taxis = new Taxi[pLista.size()];
		System.out.println(pLista);
		System.out.println(taxis);

		//		System.out.println("tamanio arreglo final:" +taxis.length+" tamanioLista"+ pLista.size());
		for (int i = 0; i<taxis.length;i++) 
		{
			taxis[i] = pLista.get(i);
		}
		System.out.println("tamanio arreglo final:" +taxis.length+" tamanioLista"+ pLista.size());
	}
	@Override
	public Taxi [] servicesInOrder() {
		Taxi [] resultado = taxis;
		HeapSort.sort(resultado);
		return resultado;
	}




	public OrderedArrayMaxPQ<Empresa> colaPrioridadTaxis(){

		Comparator<Empresa> comp = new  Comparator<Empresa>() {

			@Override
			public int compare(Empresa o1, Empresa o2) {
				// TODO Auto-generated method stub
				return o1.getServiciosRegistrados()-o2.getServiciosRegistrados();

			}
		};
		OrderedArrayMaxPQ<Empresa> rta = new OrderedArrayMaxPQ<Empresa>(taxis.length, comp);

		Taxi[] misTaxis = taxis;
		Empresa[] misEmpresas = new Empresa[misTaxis.length];
		for (int i = 0; i < misTaxis.length; i++) {
			if(misTaxis[i].getCompany()!=null)
				misEmpresas[i]= new Empresa(misTaxis[i].getCompany(),misTaxis[i].getServiciosRegistrados() );
			else
				misEmpresas[i]= new Empresa("Independent Owner", misTaxis[i].getServiciosRegistrados());

		}
		HeapSort.sort(misEmpresas);

		Empresa empresa = misEmpresas[0];
		for (int i = 1; i < misEmpresas.length; i++) {
			if( misEmpresas[i].getCompany().equals(empresa.getCompany()) ){
				empresa.addNumServiciosRegistrados(misEmpresas[i].getServiciosRegistrados());
			}
			else
			{
				rta.insert(empresa);
				empresa = misEmpresas[i];
			}
		}
		return rta;
	}






}