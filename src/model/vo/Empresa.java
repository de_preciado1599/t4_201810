package model.vo;

public class Empresa implements Comparable<Empresa>{

	

	
	/**
	 * 
	 */
	private String company;
	
	/**
	 * @return the numTaxis
	 */
	public int getNumTaxis() {
		return numTaxis;
	}


	/**
	 * @return the serviciosRegistrados
	 */
	public int getServiciosRegistrados() {
		return serviciosRegistrados;
	}

	private int numTaxis; 
	
	private int serviciosRegistrados;
	
	
	
	public Empresa( String pCompany, int servicios ){
		
		company = pCompany;
		serviciosRegistrados=servicios;
	}

	
	public String getCompany(){
		return company;
	}
	public void addNumTaxis(){
		numTaxis++;
	}
	
	public void addNumServiciosRegistrados(int pNum){
		serviciosRegistrados+=pNum;
	}


	@Override
	public int compareTo(Empresa o) {
		// TODO Auto-generated method stub
		return this.company.compareTo(o.getCompany());
	}
	
	
}
